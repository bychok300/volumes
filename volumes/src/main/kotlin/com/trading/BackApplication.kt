package com.trading

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.InjectionPoint
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Scope

@SpringBootApplication
open class BackApplication {
	companion object {
		@JvmStatic fun main(args: Array<String>) {
			SpringApplication.run(BackApplication::class.java, *args)
		}
	}

	@Bean
	@Scope("prototype")
	open fun logger(injectionPoint: InjectionPoint): Logger = LoggerFactory.getLogger(
			injectionPoint.methodParameter?.containingClass ?: injectionPoint.member.declaringClass)
}
