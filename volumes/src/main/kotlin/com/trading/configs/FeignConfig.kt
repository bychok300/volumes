package com.trading.configs

import feign.Logger
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@EnableFeignClients(basePackages = ["com.trading"])
@Configuration
open class FeignConfig {

    @Bean
    open fun feignLoggerLevel(): Logger.Level? {
        return Logger.Level.FULL
    }
}