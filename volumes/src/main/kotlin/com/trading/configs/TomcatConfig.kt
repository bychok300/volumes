package com.trading.configs

import org.apache.catalina.connector.Connector
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
open class TomcatConfig {
    @Bean
    open fun webServerFactory(): ConfigurableServletWebServerFactory? {
        val factory = TomcatServletWebServerFactory()
        factory.addConnectorCustomizers(object : TomcatConnectorCustomizer {
            override fun customize(connector: Connector) {
                connector.setProperty("relaxedQueryChars", "{}[]")
            }
        })
        return factory
    }
}

