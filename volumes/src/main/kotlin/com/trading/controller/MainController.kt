package com.trading.controller

import com.trading.dto.DepthDto
import com.trading.dto.FilteredDataRequest
import com.trading.dto.FilteredDataResponse
import com.trading.service.BinanceService
import com.trading.dto.PriceDto
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/")
class MainController @Autowired constructor(
        private val log: Logger,
        private val binanceService: BinanceService
) {

    @GetMapping("/price")
    fun getTickerPrice(@RequestParam("symbol") symbol: String): PriceDto? {
        return binanceService.getPrice(symbol)
    }

    @GetMapping("/depth")
    fun getDepth(@RequestParam("symbol") symbol: String, @RequestParam("limit") limit: Int): DepthDto? {
        return binanceService.getDepth(symbol, limit)
    }

    @PostMapping
    fun getFilteredData(@RequestBody filteredDataRequest: FilteredDataRequest): MutableList<FilteredDataResponse> {
        return binanceService.getFilteredData(filteredDataRequest)
    }
}