package com.trading.dto

data class DepthDto(
    val asks: MutableList<MutableList<String>>,
    val bids: MutableList<MutableList<String>>,
    val lastUpdateId: Long
)