package com.trading.dto

data class FilteredDataRequest(
        val tickers: MutableList<TickersDto>,
        val limit: Int
)