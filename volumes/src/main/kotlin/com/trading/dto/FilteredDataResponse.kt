package com.trading.dto

data class FilteredDataResponse(
        val name: String,
        val price: Double,
        val bidOrAsk: String,
        val values: MutableList<String>
)
