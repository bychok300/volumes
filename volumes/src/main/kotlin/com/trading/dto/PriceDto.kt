package com.trading.dto

data class PriceDto(
    val price: String,
    val symbol: String
)