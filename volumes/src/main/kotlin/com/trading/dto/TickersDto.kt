package com.trading.dto

data class TickersDto(
    val dollar_value: Double,
    val name: String
)