package com.trading.service

import com.trading.dto.DepthDto
import com.trading.dto.PriceDto
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam

@Service
@FeignClient(name = "Binance", path = "/", url = "\${feign.binance-root:}")
interface BinanceDataFetcher {

    @GetMapping("/ticker/price")
    fun getPriceByTickerName(@RequestParam("symbol") symbol: String): ResponseEntity<PriceDto>

    @GetMapping("/depth")
    fun getDepthByTickerNam(@RequestParam("symbol") symbol: String, @RequestParam("limit") limit: Int): ResponseEntity<DepthDto>

}