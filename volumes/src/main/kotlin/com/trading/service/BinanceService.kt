package com.trading.service

import com.trading.dto.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class BinanceService @Autowired constructor(
        private val binanceDataFetcher: BinanceDataFetcher
) {
    fun getPrice(symbol: String): PriceDto? {
        return binanceDataFetcher.getPriceByTickerName(symbol).body
    }

    fun getDepth(symbol: String, limit: Int): DepthDto? {
        return binanceDataFetcher.getDepthByTickerNam(symbol, limit).body
    }

    fun getFilteredData(request: FilteredDataRequest): MutableList<FilteredDataResponse> {

        val tikerList = request.tickers
        val limit = request.limit
        val result: MutableList<FilteredDataResponse> = mutableListOf()
        for (el in tikerList) {
            val tickerCurPrice = getPrice(el.name)!!.price.toDouble()
            val depth = getDepth(el.name, limit)
            val filteredAsks = depth!!.asks.filter { it[1].toDouble() >= el.dollar_value.div(tickerCurPrice) }
            val filteredBids = depth.bids.filter { it[1].toDouble() >= el.dollar_value.div(tickerCurPrice) }

            if (filteredAsks.isNotEmpty() || filteredBids.isNotEmpty()) {
                for (i in filteredAsks) {
                    i.add((i[0].toDouble() * i[1].toDouble()).toString())
                    result.add(FilteredDataResponse(
                            name = el.name,
                            price = tickerCurPrice,
                            bidOrAsk = "ask",
                            values = i))
                }

                for (i in filteredBids) {
                    i.add((i[0].toDouble() * i[1].toDouble()).toString())
                    result.add(FilteredDataResponse(
                            name = el.name,
                            price = tickerCurPrice,
                            bidOrAsk = "bid",
                            values = i))
                }
            }
        }
        return result
    }

}